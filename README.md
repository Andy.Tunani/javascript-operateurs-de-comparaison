# Javascript - Opérateurs de comparaison

**Opérateurs de comparaison** : permet de comparer 2 éléments entre eux
dans le CSS

**Example d'Opérateurs de comparaison :**

_**Égalité simple**_ :

    // Égalité simple

    let nb1 = 10

    let nb2 = 12

    console.log('Égalité simple :', nb1 == nb2);

    réponse : true ou false

_**Inégalité simple**_ :

    // Inégalité simple

    let nb3 = 30

    let nb4 = "19"

    console.log('Égalité simple :', nb3 != nb4);

**_Égalité strict_**

    // Égalité strict

    let nb5 = 10

    let nb6 = 10

    console.log('Égalité strict :', nb5 === nb6);

**_Inégalité strict_**

    // Inégalité strict

    let nb7 = 30

    let nb8 = 30

    console.log('Inégalité strict :', nb7 !== nb8);

**_Strictement superieur_**

    // Strictement superieur

    let nb9 = 30

    let nb10 = 30

    console.log('Strictement superieur :', nb9 > nb10);

_**Strictement infèrieur**_ : 

    // Strictement infèreur 

    let nb11 = 10

    let nb12 = 10

    console.log('Strictement superieur :', nb9 < nb10);

_________

**Exemple d'Opérateurs Logique :**

**_ET Logique =>   expression1  &&  expression2_**

*si une expression est fausse, toute l'expression sera refusée.

    let nb1 = 2
    let nb2 = 2
    let disable = false
    let loading = true
    let name = "Jean"
    
    console.log('ET Logique :', nb1 === nb2 && name === "Jean");

_**OU Logique =>   expression1  ||  expression2**_

*si une expression est correcte, toute l'expression sera validée.

    let price = 499
    let adult = true
    let city = "Paris"

    console.log('OU Logique :', price > 300 || adult || city === "Paris");

**_NON Logique =>   !expression1_**

*nous donne l'inverse de l'expression.

    let nullvar = null
    let undefinedVar; // undefined
    let pro = false
    let count = 0
    let str = ""
    
    console.log('NON Logique :', !nullvar, !undefinedVar, !pro, !count, !str);

!!str
str !=  null && str == undefined && str != "" 

**_Mixed_**

*si une expression est fausse, toute l'expression sera refusée.

    let nb3 = 3
    let nb4 = "4"
    let shown = true
    let empty

    console.log('Mixed :', ((true && true) && || true) && !false && 3 > 1);












